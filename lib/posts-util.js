import matter from 'gray-matter';
import fs from 'fs';
import path from 'path';

const postsDir = path.join(process.cwd(), 'content', 'posts');

function getPostsFiles() {
  return fs.readdirSync(postsDir);
}

function getPostData(postIdentifier) {
  const postSlug = postIdentifier.replace(/\.md$/, '');
  const filePath = path.join(postsDir, `${postSlug}.md`);
  const fileContent = fs.readFileSync(filePath, 'utf-8');

  const { data, content } = matter(fileContent);

  const postData = {
    slug: postSlug,
    ...data,
    content,
  };

  return postData;
}

function getAllPosts() {
  const postFiles = getPostsFiles();

  const allPosts = postFiles.map((postFile) => getPostData(postFile));

  const sortedPosts = allPosts.sort((postA, postB) => (postA.date > postB.date ? -1 : 1));

  return sortedPosts;
}

function getFeaturePosts() {
  const allPosts = getAllPosts();
  const featurePosts = allPosts.filter((post) => post.isFeatured);

  return featurePosts;
}

export { getFeaturePosts, getAllPosts, getPostData, getPostsFiles };
