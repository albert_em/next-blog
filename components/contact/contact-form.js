import { useEffect, useState } from 'react';
import classes from './contact-form.module.css';

import Notification from '../../components/ui/notification';

async function sendContactData(formData) {
  const response = await fetch('/api/contact', {
    method: 'POST',
    body: JSON.stringify(formData),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || 'Something wrong');
  }
}

export default function ContactForm() {
  const [formData, setFormData] = useState({ email: '', name: '', message: '' });
  const [requestStatus, setRequestStatus] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const onChangeForm = (event) => {
    const { id, value } = event.target;
    setFormData((prevData) => ({ ...prevData, [id]: value }));
  };

  async function sendMessageHandler(event) {
    event.preventDefault();
    setRequestStatus('pending');

    try {
      await sendContactData(formData);
      setRequestStatus('success');
      setFormData({ email: '', name: '', message: '' });
    } catch (error) {
      setErrorMessage(error.message);
      setRequestStatus('error');
    }
  }

  useEffect(() => {
    if (requestStatus === 'success' || requestStatus === 'error') {
      const timer = setTimeout(() => {
        setRequestStatus(null);
        setErrorMessage(null);
      }, 3000);
      return () => clearTimeout(timer);
    }
  }, [requestStatus]);

  let notification;

  if (requestStatus === 'pending') {
    notification = {
      status: requestStatus,
      title: 'Sending message',
      message: 'Your message is on its way',
    };
  }

  if (requestStatus === 'success') {
    notification = {
      status: requestStatus,
      title: 'Success',
      message: 'Message sent successfully',
    };
  }

  if (requestStatus === 'error') {
    notification = {
      status: requestStatus,
      title: 'Error',
      message: errorMessage,
    };
  }

  return (
    <section className={classes.contact}>
      <h1>How can I help you?</h1>
      <form className={classes.form} onSubmit={sendMessageHandler} onChange={onChangeForm}>
        <div className={classes.controls}>
          <div className={classes.control}>
            <label htmlFor='email'>Your Email</label>
            <input type='email' id='email' required />
          </div>

          <div className={classes.control}>
            <label htmlFor='email'>Your Name</label>
            <input type='text' id='name' required />
          </div>
        </div>
        <div htmlFor='message'>
          <label htmlFor='message'>Your message</label>
          <textarea id='message' row='5' required></textarea>
        </div>
        <div className={classes.actions}>
          <button>Send Message</button>
        </div>
      </form>
      {notification && <Notification {...notification} />}
    </section>
  );
}
