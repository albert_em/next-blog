import Image from 'next/image';
import classes from './hero.module.css';

export default function Hero() {
  return (
    <section className={classes.hero}>
      <div className={classes.image}>
        <Image src='/images/site/blockies.png' alt='An image showing me' width={300} height={300} quality={100} />
      </div>
      <h1>Hi, I'm Albert</h1>
      <p>I blog about web development - especcially frontend frameworks</p>
    </section>
  );
}
