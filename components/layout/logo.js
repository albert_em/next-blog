import Link from 'next/link';
import classes from './logo.module.css';

export default function Logo() {
  return (
    <Link href='/'>
      <div className={classes.logo}>Albert's blog</div>
    </Link>
  );
}
