import Link from 'next/link';
import Image from 'next/image';

import classes from './posts-item.module.css';

export default function PostItem({ title, image, excerpt, date, slug }) {
  const readebleDate = new Date(date).toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  });

  const imagePath = `/images/posts/${slug}/${image}`;
  const href = `/posts/${slug}`;

  return (
    <li className={classes.post}>
      <Link href={href}>
        <div className={classes.image}>
          <Image src={imagePath} alt={title} width={300} height={200} priority layout='responsive' />
        </div>
        <div className={classes.content}>
          <h3>{title}</h3>
          <time>{readebleDate}</time>
          <p>{excerpt}</p>
        </div>
      </Link>
    </li>
  );
}
