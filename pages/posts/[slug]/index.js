import { Fragment } from 'react';
import PostContent from '../../../components/posts/post-details/post-content';
import { getPostData, getPostsFiles } from '../../../lib/posts-util';
import Head from 'next/head';

export default function PostDetails({ post }) {
  return (
    <Fragment>
      <Head>
        <title>{post.title}</title>
        <meta name='description' content={post.excerpt} />
      </Head>
      <PostContent {...post} />;
    </Fragment>
  );
}

export function getStaticProps(context) {
  const { params } = context;

  const post = getPostData(params.slug);

  return {
    props: {
      post,
    },
    revalidate: 600,
  };
}

export function getStaticPaths() {
  const postFileNames = getPostsFiles();

  const slugs = postFileNames.map((fileName) => fileName.replace(/\.md$/, ''));

  return {
    paths: slugs.map((slug) => ({ params: { slug } })),
    fallback: false,
  };
}
