import { Fragment } from 'react';
import Head from 'next/head';

import Hero from '../components/home-page/hero';
import FeaturedPosts from '../components/home-page/featured-posts';
import { getFeaturePosts } from '../lib/posts-util';

export default function Home({ posts }) {
  return (
    <Fragment>
      <Head>
        <title>Weclome to my Blog</title>
        <meta name='description' content='I post about programming and web development' />
      </Head>
      <Hero />
      <FeaturedPosts posts={posts} />
    </Fragment>
  );
}

export function getStaticProps() {
  const featuredPosts = getFeaturePosts();

  return {
    props: {
      posts: featuredPosts,
    },
    revalidate: 60,
  };
}
